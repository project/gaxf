<?php
?>
<div id="gaxf-slider-container-wrapper">
  <div id="gaxf-slider-container">

    <!-- HTML structures -->
    <div id="actions">
        <a class="prev">&laquo; Back</a>
        <a class="next">More pictures &raquo;</a>
    </div>

    <!-- root element for scrollable -->
    <div class="scrollable vertical">
      <!-- root element for the items -->
      <div class="items">

        <?php foreach ($data2 as $scroll) { ?>
        <div>
          <?php foreach ($scroll as $item) { ?>
          <div class="item">
              <div class="gaxf-image-box"><?php print $item->thumbnail; ?></div>
              <h3><?php print $item->ptitle_link ?></h3>
              <?php if($item->copyright) { ?>
              <p>
                © <?php print $item->copyright ?>
              </p>
              <?php } ?>
              <?php if($item->pdescription) { ?>
              <p>
                <?php print $item->pdescription ?>
              </p>
              <?php } ?>
              <?php if($item->read_more) { ?>
              <p>
                  <?php print $item->read_more_link ?>
              </p>
              <?php } ?>
          </div>
          <?php } ?>
        </div>
        <?php } ?>
        
      </div>
    </div>
  </div>
</div>



