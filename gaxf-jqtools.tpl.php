<?php
?>
<div id="gaxf-slider-container-wrapper">
 <div id="gaxf-slider-container">
   <!-- wrapper element for the large image -->
   <div id="image_wrap">
   
    <!-- Initially the image is a simple 1x1 pixel transparent GIF -->
    <img src="http://static.flowplayer.org/tools/img/blank.gif" width="500" height="375" />
   
   </div>
   
   <!-- wrapper for navigator elements -->
   <div class="navi"></div>
   
   <div style="display: inline-block;margin:0 auto;">
   
   <!-- "previous page" action -->
   <a class="prev browse left"></a>
   
   <!-- root element for scrollable -->
   <div class="scrollable" id=scroller>   
      
      <!-- root element for the items -->
      <div class="items">
   <?php 
     foreach ($data as $item) {
       print "<div><img src=\"". file_create_url($item->tpath) ."\" /></div>";
     } 
   ?>
      </div>
      
   </div>
   
   <!-- "next page" action -->
   <a class="next browse right"></a>
   
   </div>
   
   <br clear="all" />
   
   <div id="actionButtons" style="display: none;">
     <button type="button" onClick="api.play()">Play</button>
     <button type="button" onClick="api.pause()">Pause</button>
     <button type="button" onClick="api.stop()">Stop</button>
   </div>
 </div>
</div>
