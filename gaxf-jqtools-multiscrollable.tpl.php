<?php
?>
<div id="gaxf-slider-container-wrapper">
  <div id="gaxf-slider-container">

    <!-- scroll #1 -->
    <div class="scroll">
     <div class="pics">
       <?php foreach ($data as $item) { ?>
       <div style="background-image:url(<?php print file_create_url($item->ppath); ?>)"></div>
       <?php } ?>
     </div>
    </div>

  </div>
</div>

<div class="clear-block"></div>
