<?php
?>
<div id="gaxf-slider-container-wrapper">
  <div id="gaxf-slider-container">

    <?php for ($i=0;$i<4;++$i) { ?>
    <!-- scroll #<?php print $i; ?> -->
    <div class="scroll">
     <div class="pics">
       <?php 
         $ii = 0;
         shuffle($data);
         foreach ($data as $item) { 
           ++$ii; ?>
       <div style="background-image:url(<?php print file_create_url($item->ppath); ?>)"></div>
       <?php 
           unset($data[$item->pid]);
           if ($ii == 5) {break;} 
         } ?>
     </div>
    </div>
    <?php } ?>

  </div>
</div>

<div class="clear-block"></div>
