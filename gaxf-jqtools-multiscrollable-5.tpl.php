<?php
?>
<div id="gaxf-slider-container-wrapper">
  <div id="gaxf-slider-container">

    <?php foreach ($data2 as $scroll) { ?>
    <div class="scroll">
     <div class="pics">
       <?php foreach ($scroll as $item) { ?>
       <div style="background-image:url(<?php print file_create_url($item); ?>)"></div>
       <?php } ?>
     </div>
    </div>
    <?php } ?>

  </div>
</div>

<div class="clear-block"></div>
