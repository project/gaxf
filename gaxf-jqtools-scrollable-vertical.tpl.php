<?php
?>
<div id="gaxf-slider-container-wrapper">
  <div id="gaxf-slider-container">

    <!-- HTML structures -->
    <div id="actions">
        <a class="prev">&laquo; Back</a>
        <a class="next">More pictures &raquo;</a>
    </div>

    <!-- root element for scrollable -->
    <div class="scrollable vertical">
      <!-- root element for the items -->
      <div class="items">
        <?php foreach ($data as $item) { ?>
        <div class="item">
            <!--<img src="http://farm1.static.flickr.com/3650/3323058611_d35c894fab_m.jpg" />-->
            <div class="gaxf-image-box"><?php print $item->thumbnail; ?></div>
            <h3><?php print $item->ptitle_link ?></h3>
              <?php if($item->copyright) { ?>
              <strong>
                © <?php print $item->copyright ?>
              </strong>
              <?php } ?>
              <?php if($item->pdescription) { ?>
              <p>
                <?php print $item->pdescription ?>
              </p>
              <?php } ?>
            <?php if($item->read_more) { ?>
            <p>
                <?php print $item->read_more_link ?>
            </p>
            <?php } ?>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
