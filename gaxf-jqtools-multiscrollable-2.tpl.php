<?php
?>
<div id="gaxf-slider-container-wrapper">
  <div id="gaxf-slider-container">

    <?php for ($i=0;$i<2;++$i) { ?>
    <!-- scroll #<?php print $i; ?> -->
    <div class="scroll">
     <div class="pics">
       <?php 
         $ii = 0;
         while ($item = array_pop($data)) { 
           ++$ii; ?>
       <div style="background-image:url(<?php print file_create_url($item->ppath); ?>)"></div>
       <?php if ($ii == 5) {break;} } ?>
     </div>
    </div>
    <?php } ?>

  </div>
</div>

<div class="clear-block"></div>
