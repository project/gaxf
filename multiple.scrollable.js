
$(function() {

  // enable circular scrollables with a click handler
  $(".scroll").scrollable({ circular: true }).click(function() {
    $(this).data("scrollable").next();		
  });

});
